from flask import Flask, Response, stream_with_context, render_template, flash, request, url_for, redirect, json, session, jsonify, abort
from functools import wraps
from threading import Lock
from flask_socketio import SocketIO, emit, join_room, leave_room, close_room, rooms, disconnect
from flask_oauthlib.client import OAuth, OAuthException
from provision import WindowsDeployer
import uuid

import time
import json
import random


roles = json.load(open("permissions.json"))
async_mode = None

app = Flask(__name__)
app.config['SECRET_KEY'] = 'secret!'
oauth = OAuth(app)

socketio = SocketIO(app, async_mode=async_mode)
thread = None
thread_lock = Lock()


#OAuth info for signing in
microsoft = oauth.remote_app(
	'microsoft',
	consumer_key='61679d5e-266b-4c95-a129-96a7a34df9a8',
	consumer_secret='pjW714]^;indnlBZVFUJ51^',
	request_token_params={'scope': 'offline_access User.Read'},
	base_url='https://graph.microsoft.com/v1.0/',
	request_token_url=None,
	access_token_method='POST',
	access_token_url='https://login.microsoftonline.com/common/oauth2/v2.0/token',
	authorize_url='https://login.microsoftonline.com/common/oauth2/v2.0/authorize'
)

def generate():
    global thread
    data=["Checking parameters",
        "Allocating resources",
        "Performing configuration",
        "Applying Security",
        "Cleaning up",
        "Done!"]
    for item in data:
        socketio.emit('my_response',
                    {'data': item, 'count': 0},
                    namespace='/console')
        socketio.sleep(random.randint(1, 5))
    #thread = None

#Defines the language of the site (multilanguage support, could be fetched from user token)
lang = "en"
brand = "aia" # default value "default"





def login_required(f):
    """ 
    Ensures that the user is logged in, if not, takes the user to a login page\n
    This is using AAD with @Spydernaz domain
    
    @wraps(f)
    def wrap(*args, **kwargs):
        if 'microsoft_token' in session:
            return f(*args, **kwargs)
        else:
            return redirect(url_for('login'))
    return wrap
    """
    @wraps(f)
    def wrap(*args, **kwargs):
        if 'logged_in' in session:
            return f(*args, **kwargs)
        else:
            abort(401)
    return wrap



#####################################################################################           Home Page
#   Home Page
##########################################

@app.route('/')
@login_required
def homepage():
    """Returns the homepage\n
    @TODO: Define content to go on the homepage, currently blank
    """
    return render_template("/{}/subpages/home.html".format(lang), menuHome = "active", brand = brand)


#####################################################################################           Provision New Resource Page
#   Provision a new resource
##########################################
@app.route('/new')
@login_required
def provisionnewresource():
    """
    Displays all the components that the user can provision and provides a form for the necessary details\n
    @TODO: Get the components from a session token and compare to JSON (or DB etc)\n
    @TODO: have the resources hand the config to an API for provisioing
    """
    role = session['role']
    return render_template("/{}/subpages/provision/newresource.html".format(lang),
                lang = lang,
                menuNew = "active",
                brand = brand,
                # @TODO: Get the components from a session token and compare to JSON (or DB etc)
                components=roles[role])


#####################################################################################           Console
#   Console Test Application
##########################################
@app.route('/provision', methods=['POST'])
def provision():
    """Essentially the API for provisioing\n
    @TODO: Implement a basic provisioning based on an API call/form from the web interface\n
    @TODO: Ensure that the user requesting has permissions (esp. in the case of an api call)\n
    @TODO: Set a new thread for provisioing and create a new web socket for the user to listen to\n
    @TODO: Add the asset to an asset register so the user can see all resources they provisioned\n
    @TODO: Remove the random generate() function\n
    @TODO: get the provisioing to work against methods in other classes
    """
    try:
        params = {}
        params['subscription_id'] = '5d6a75d8-acd6-4f2e-97c9-cc86702dc57e'
        params['vmName'] = request.form['pn']
        params["location"] = "australiaeast"
        params = json.dumps(params)

        print(params)

        deployer = WindowsDeployer(json.loads(params))
        deployer.deploy()

        return redirect(url_for("stream_view"))
    except:
        abort(500)

@app.route('/stream_view')
def stream_view():
    """ Renders the output of the machines provisioning """
    return render_template("/{}/subpages/test/console.html".format(lang),
                rows=["rows"],
                menuConsole="active",
                brand = brand,
                async_mode=socketio.async_mode)









#@TODO: Login against AAD/AD

#####################################################################################           Login Page
#   Login page and function
##########################################
"""@app.route("/login", methods=['POST', 'GET'])
    def login():
        _user = "Username"
        # Set the user's role in cookies
        session['role'] = "other"

        guid = uuid.uuid4()
        session['state'] = guid

        return microsoft.authorize(callback=url_for('authorized', _external=True), state=guid)
"""

@app.route("/login", methods=['POST','GET'])
def login():
    if request.method == 'GET':
        return render_template("/{}/subpages/login.html".format(lang),
                brand = brand)
    else:
        # if request.form['psw'] == 'password' and request.form['uname'] == 'admin':
        session['logged_in'] = True
        session['role'] = request.form['uname']
            
    return redirect(url_for("homepage"))

#####################################################################################           Logout Page
#   Logout page and function
##########################################
@app.route("/logout", methods=["GET","POST"])
def logout():
    #session.pop('microsoft_token', None)
    #session.pop('state', None)
    session.pop('logged_in', None)
    session.pop('role', None)
    return redirect(url_for('login'))


@app.route('/login/authorized')
def authorized():
	response = microsoft.authorized_response()

	if response is None:
		return "Access Denied: Reason=%s\nError=%s" % (
			response.get('error'), 
			request.get('error_description')
		)
		
	# Check response for state
	print("Response: " + str(response))
	if str(session['state']) != str(request.args['state']):
		raise Exception('State has been messed with, end authentication')
		
	# Okay to store this in a local variable, encrypt if it's going to client
	# machine or database. Treat as a password. 
	session['microsoft_token'] = (response['access_token'], '')

	return redirect(url_for('me')) 

@app.route('/me')
def me():
	me = microsoft.get('me')
	return render_template('me.html', me=str(me.data))

@microsoft.tokengetter
def get_microsoft_oauth_token():
	return session.get('microsoft_token')












#####################################################################################           Logout Page
#   Error Handling Functions
##########################################
@app.errorhandler(404)
def notfound(e):
    return render_template("{}/errors/404.html".format(lang), brand = brand)

@app.errorhandler(401)
def unauthorised(e):
    return render_template("{}/errors/401.html".format(lang), brand = brand)
    
@app.errorhandler(500)
def servererror(e):
    return render_template("{}/errors/500.html".format(lang), brand = brand)









### Run the App ###
if __name__ == "__main__":
    socketio.run(app, debug=True, port=5000)
