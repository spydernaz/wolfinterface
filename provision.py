"""A deployer class to deploy a template on Azure"""
import os.path
import json
from azure.common.credentials import ServicePrincipalCredentials
from azure.mgmt.resource import ResourceManagementClient
from azure.mgmt.resource.resources.models import DeploymentMode

class WindowsDeployer(object):
    """ Initialize the deployer class with subscription, resource group and public key.

    :raises IOError: If the public key path cannot be read (access or not exists)
    :raises KeyError: If AZURE_CLIENT_ID, AZURE_CLIENT_SECRET or AZURE_TENANT_ID env
        variables or not defined
    """

    def __init__(self, params):
        self.subscription_id = params["subscription_id"]
        self.vmName = params["vmName"]
        self.location = params["location"]
        self.resource_group = "{}-rg".format(self.vmName)

        self.credentials = ServicePrincipalCredentials(
            client_id="a8343a9f-8562-4955-9722-934006ada75f",
            secret="7ja61ZDUZJU6Kkz3UMBvxEAj5mBlV4SO7nn/ZMlvxsE=",
            tenant="13a4d480-9b2c-43f4-87a9-6f5148ea6b02"
        )
        self.client = ResourceManagementClient(self.credentials, self.subscription_id)

    def deploy(self):
        """Deploy the template to a resource group."""
        self.client.resource_groups.create_or_update(
            self.resource_group,
            {
                'location':self.location
            }
        )

        template_path = os.path.join(os.path.dirname(__file__), 'ARM', 'DSS2016.json')
        with open(template_path, 'r') as template_file_fd:
            template = json.load(template_file_fd)

        parameters = {
            'virtualMachineName': self.vmName
        }
        parameters = {k: {'value': v} for k, v in parameters.items()}

        deployment_properties = {
            'mode': DeploymentMode.incremental,
            'template': template,
            'parameters': parameters
        }

        deployment_async_operation = self.client.deployments.create_or_update(
            self.resource_group,
            self.vmName,
            deployment_properties
        )
        deployment_async_operation.wait()

    def destroy(self):
        """Destroy the given resource group"""
        self.client.resource_groups.delete(self.resource_group)
