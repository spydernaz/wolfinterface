Self Service Portal
======

Serving Wolf on a silver platter

## Concept: ##

To provide an open source web self service protal that we can customise and control. It is built in Python to support more flexibility and future development.

## Objectives ##

### Authentication ###
Functional Area | Sub Item | WebApp | API
:--- | :--- | :--- | :---: | :---: |
Authentication | Login via LDAP (AD) | | 
Authentication | Login via OAuth (AAD) | &#9989; |
Authentication | Protected Methods/Pages | &#9989; |
Authentication | Login for API | |
Authentication | Timeout user sessions | |
Authentication | Use SSL certificates | | -


### Authorisation ###
Functional Area | Sub Item | WebApp | API
:--- | :--- | :--- | :---: | :---: |
Authorisation | Programatically allow users to particular functions | |
Authorisation | Introduce approval workflow | | - [ ]


### Provisioning ###
Functional Area | Sub Item | WebApp | API
:--- | :--- | :--- | :---: | :---: |
Provisiong | Execute a `.ps1` script | |
Provisiong | Execute a `.sh` script | |
Provisiong | Execute a `.py` script | |
Provisiong | Display the output of a script | &#9989; | &#10060;
Provisiong | Execute a script via API | |


### Manage a Resource ###
Functional Area | Sub Item | WebApp | API
:--- | :--- | :--- | :---: | :---: |
Manage | Turn my resource on/off (if applicable) | |
Manage | Remove / Delete my resource | |
Manage | Adjust the size of my resource | |
Manage | Add new components to my resource (if applicable) | |
Manage | Execute auxilary methods (i.e. add a user) | |


### Service Outages / Notifications ###
Functional Area | Sub Item | WebApp | API
:--- | :--- | :--- | :---: | :---: |
Outages | Have a notifaction view for users | |
Outages | automatically source outages from servce provider | |
Notifications | Notify the user of actions specific to them (i.e. lab finished provisioing) | |
Notifications | Make notifications dismissable and actionable | |

