$(document).ready(function(){

    $('#messageSend').click(function(){
        var inputMessage = document.getElementById('messageInput').value;
        document.getElementById('messageInput').value = null;


        //Create the message in the conversation
        var messageContainer = document.createElement('div');
        messageContainer.className = 'msc';

        var frame = document.getElementById('conversationContainer');
        frame.appendChild(messageContainer);


        var message = document.createElement('p');
        message.className = 'messageSent';
        message.innerHTML = inputMessage;
        
        messageContainer.appendChild(message);

        // Create the JS call

        //display the response

        console.log(inputMessage);
    });
});